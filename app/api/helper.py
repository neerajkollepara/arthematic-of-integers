
def add_binary_nums(x, y, leng):
    """ This function gives the binary representation after addition of the given two input binary strings. """
    result = ''
    carry = 0
    for i in range(leng-1, -1, -1):
        r = carry
        r += 1 if x[i] == '1' else 0
        r += 1 if y[i] == '1' else 0
        result = ('1' if r % 2 == 1 else '0') + result
        carry = 0 if r < 2 else 1
    if carry != 0:
        result = '1' + result
    return result

def getval(x, l):
    """ This function gives the signed decimal value of the given binary string. """
    if l == 4:
        if int(x, 2) == 0 or int(x, 2) == 8:
            return 0
    elif l == 5:
        if int(x, 2) == 0 or int(x, 2) == 16:
            return 0
    if x[0] == '1':
        num = int(x, 2)
        num = num-16
    elif x[0] == '0':
        num = int(x, 2)
    return num

def getonescom(x, l):
    """ This function gives the ones compliment of the given binary number. """
    if l == 4:
        if int(x, 2) == 0 or int(x, 2) == 8:
            return 0
    elif l == 5:
        if int(x, 2) == 0 or int(x, 2) == 16:
            return 0
    ans = ''
    if x[0] == '1':
        for i in range(1, l, 1):
            if x[i] == '1':
                ans = '0' + ans
            elif x[i] == '0':
                ans = '1' + ans
        num = int(ans, 2)
        num = num-8
    elif x[0] == '0':
        for i in range(1, l, 1):
            if x[i] == '0':
                ans = '0' + ans
            elif x[i] == '1':
                ans = '1' + ans
        num = int(ans, 2)
    return num

def gettwoscom(x, l):
    """ This function gives the twos compliment of the given binary number. """
    if l == 4:
        if int(x, 2) == 0 or int(x, 2) == 8:
            return 0
    elif l == 5:
        if int(x, 2) == 0 or int(x, 2) == 16:
            return 0
    num = getonescom(x, l)
    num += 1
    return num

def getinf(a, l):
    """ This function gives the information of the particular binary string. """
    value = getval(a, l)
    print(value)
    one = getonescom(a, l)
    print(one)
    two = gettwoscom(a, l)
    print(two)

def add_ones(a, b, l):
    """ This function gives the sum of ones compliment of given two input binary strings. """
    num1 = getonescom(a, l)
    num2 = getonescom(b, l)
    return num1+num2

def add_twos(a, b, l):
    """ This function gives the sum of twos compliment of given two input binary strings. """
    num1 = gettwoscom(a, l)
    num2 = gettwoscom(b, l)
    return num1+num2

def signed_sum(a, b, l):
    """ This function gives the signed sum of given two binary numbers. """
    num1 = getval(a, l)
    num2 = getval(b, l)
    return num1+num2

def unsigned_sum(a, b, l):
    """ This function gives the unsigned sum of given two binary numbers. """
    if l == 4 or l == 5:
        num1 = int(a, 2)
        num2 = int(b, 2)
        return num1+num2

def calg0(p, a, b):
    """ This function returns required answer. """
    return add_binary_nums(str(a), str(b), int(p))
def calg1(p, a, b):
    """ This function returns required answer. """
    return add_ones(str(a), str(b), int(p))
def calg2(p, a, b):
    """ This function returns required answer. """
    return add_twos(str(a), str(b), int(p))
def calg3(p, a, b):
    """ This function returns required answer. """
    return signed_sum(str(a), str(b), int(p))
def calg4(p, a, b):
    """ This function returns required answer. """
    return unsigned_sum(str(a), str(b), int(p))
