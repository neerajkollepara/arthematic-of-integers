import datetime
from flask import Flask
from controller import main
from flask_sqlalchemy import SQLAlchemy
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///test.db'
db = SQLAlchemy(app)
db.init_app(app)
#db.create_all()

if __name__ == '__main__':
    db.create_all()
    app.run(debug=True)
