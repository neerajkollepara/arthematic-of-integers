
function calculate()
{
  var length = document.getElementById('primeno')
  var number1 = document.getElementById('generator1')
  var number2 = document.getElementById('generator2')
  var binarybox = document.getElementById('answer0')
  var onesbox = document.getElementById('answer1')
  var twosbox = document.getElementById('answer2')
  var signed = document.getElementById('answer3')
  var unsigned = document.getElementById('answer4')
  fetch("/api/calg?p="+length.value+"&a="+number1.value+"&b="+number2.value)
  .then((res) => res.json())
  .then(res => {
    binarybox.value=res.r
    onesbox.value=res.Ga
    twosbox.value=res.Gb
    signed.value=res.Gab
    unsigned.value=res.Gba
  });
}
