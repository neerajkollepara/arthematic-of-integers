from flask import Blueprint, render_template, request
from api import helper
from models import Quiz
from models import db
import json
import jsonify

main=Blueprint('main', __name__)

@main.route('/')
def index():
    return render_template("Introduction.html")

@main.route('/introduction')
def introduction():
    return render_template("Introduction.html")
@main.route('/theory')
def theory():
    return render_template("Theory.html")

@main.route('/objective')
def objective():
    return render_template("Objective.html")

@main.route('/experiment')
def experiment():
    return render_template("Experiment.html")

@main.route('/quizzes')
def quizzes():
    return render_template("Quizzes.html")

@main.route('/procedure')
def procedure():
    return render_template("Procedure.html")

@main.route('/further_reading')
def further_reading():
    return render_template("Further Readings.html")
    pass

@main.route('/api/calg')
def calg():

    p=int(str(request.args.get('p')))
    a=str(request.args.get('a'))
    b=str(request.args.get('b'))

    r = str(helper.calg0(p,a,b))
    Ga = int(str(helper.calg1(p,a,b)))
    Gb = int(str(helper.calg2(p,a,b)))
    Gab = int(str(helper.calg3(p,a,b)))
    Gba = int(str(helper.calg4(p,a,b)))

    ret = {
        'r' : r,
        'Ga' : Ga,
        'Gb' : Gb,
        'Gab' : Gab,
        'Gba' : Gba,
    }

    return json.dumps(ret)

@main.route('/addanswers', methods = ['POST'])
def addAnswers():
    try:
        answer1 = request.form['q1']
        answer2 = request.form['q2']
        answer3 = request.form['q3']
        answer4 = request.form['q4']
    except:
        flash('Values not obtained',"error")
    Answer = Quiz(answer1, answer2, answer3, answer4)
    db.create_all()    #sdfa
    db.session.add(Answer)
    db.session.commit()
    return jsonify(state=True)
